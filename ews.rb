#!/usr/bin/env ruby

require "awesome_print"
require "yaml"
require "exchanger"

def main
  Exchanger::Config.instance.from_hash(YAML.load_file("config/exchange.yml"))
  folder = Exchanger::Folder.find(:contacts)
  folder.contacts.each do |contact|
    ap contact.display_name
    ap contact.email_addresses
  end

  contact = folder.new_contact
  contact.given_name = "Mr."
  contact.surname = "Fridge"
  contact.email_addresses = [ Exchanger::EmailAddress.new(:key => "EmailAddress1", :text => "me@example.com") ]
  contact.phone_numbers = [ Exchanger::PhoneNumber.new(:key => "MobilePhone", :text => "+371 80000000") ]
  contact.save # CreateItem operation
  contact.company_name = "SR Inc."
  contact.save # UpdateItem operation
  # contact.destroy # DeleteItem operation
end

# As executable - main
if __FILE__ == $PROGRAM_NAME
  main
end
